#!/usr/bin/python3

import os

from flask import Flask
from flask import render_template, request

import calculators
import data

app = Flask(__name__)


@app.route("/")
def form():
    return render_template("form.html")


@app.route("/", methods=["POST"])
def my_form_post():
    weight = request.form["weight"]
    feet = request.form["feet"]
    inches = request.form["inches"]

    bmi = calculators.bmi_calculator(int(feet), int(inches), int(weight))
    health = calculators.health_calculator(bmi).name

    # goal = request.form["goal"]

    return render_template("form.html", weight=weight, bmi=bmi, health=health)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))

